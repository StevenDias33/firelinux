cron="*/5 * * * * rsync -e 'ssh -o StrictHostKeyChecking=no' -av /var/www/html/data/* vagrant@192.168.12.11:/var/www/html/data/"
cron_escaped=$(echo "$cron" | sed s/\*/\\\\*/g)
crontab -l | grep "${cron_escaped}"
if [[ $? -eq 0 ]] ;
  then
    echo "Crontab already exists. Exiting..."
    exit
  else
    crontab -l > mycron
    echo "$cron" >> mycron
    crontab mycron
    rm mycron
fi